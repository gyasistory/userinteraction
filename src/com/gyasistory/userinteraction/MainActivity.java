package com.gyasistory.userinteraction;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.gyasistory.Layouts.CategoryRadio;
import com.gyasistory.lib.Storage;
import com.gyasistory.lib.WebFileStuff;
import com.gyasistory.quotes.InspirationaQuote;
import com.gyasistory.quotes.Love;

public class MainActivity extends Activity {

	WebFileStuff _webFile;
	public static Context _context;
	TextView resultText;
	int _rId;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Set Variable
		_context = this;

		// Layout of App
		LinearLayout ll = new LinearLayout(this);
		ll.setOrientation(LinearLayout.VERTICAL);
		ll.setPadding(5, 5, 5, 5);
		LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.MATCH_PARENT,
				LinearLayout.LayoutParams.MATCH_PARENT);
		ll.setLayoutParams(lp);

		// Title
		TextView title = new TextView(this);
		title.setText("Daily Quotes");
		title.setBackgroundColor(Color.DKGRAY);
		title.setTextColor(Color.LTGRAY);
		title.setGravity(Gravity.CENTER);
		title.setTextSize(50);
		ll.addView(title);

		// Text Pick a Category
		TextView byDateText = new TextView(this);
		byDateText.setText("Select a Quote by Category");
		byDateText.setTextSize(20);
		byDateText.setPadding(0, 10, 0, 5);
		ll.addView(byDateText);

		// Add Radio Group
		RadioGroup group = new RadioGroup(this);
		CategoryRadio.CategoryRadio(this);
		group.addView(CategoryRadio.catGroup);
		ll.addView(group);

		// add Button
		Button b = new Button(this);
		b.setText("Get New Quote");
		b.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// Test connection
				WebFileStuff.getConnStatus(_context);
				WebFileStuff.getConnType(_context);
				_rId = CategoryRadio.catGroup.getCheckedRadioButtonId();

				Log.i("Checked ID", Integer.toString(_rId));
				if (WebFileStuff._conn) {
					switch (_rId) {
					// Inspirational Results
					case 1:
						InspirationaQuote quote = new InspirationaQuote();
						String inspirText = quote.getQuoteText();
						resultText.setText(inspirText);

						break;

					// Love Results
					case 2:
						Love loveQuote = new Love();
						String loveText = loveQuote.getQuoteText();
						resultText.setText(loveText);
						break;

					// Friendship Results
					case 3:
						Love FriendQuote = new Love();
						String friendText = FriendQuote.getQuoteText();
						resultText.setText(friendText);
						break;

					default:
						resultText.setText("");
						break;
					}
				}

			}
		});

		ll.addView(b);

		Button viewSaved = new Button(_context);
		viewSaved.setText("View Saved Quote");
		viewSaved.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				_rId = CategoryRadio.catGroup.getCheckedRadioButtonId();
				switch (_rId) {
				case 1:
					String savedText = Storage.readString(_context,
							"Inspirational", false);
					resultText.setText(savedText);
					break;

				case 2:
					resultText.setText(Storage.readString(_context, "Love",
							true));
					break;

				case 3:
					resultText.setText(Storage.readString(_context,
							"Friendship", false));
					break;

				default:
					Toast st = Toast.makeText(_context, "Select a Category",
							Toast.LENGTH_LONG);
					st.show();
					break;
				}

			}
		});
		ll.addView(viewSaved);

		Button downloadButton = new Button(_context);
		downloadButton.setText("Save Quote");
		downloadButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				String savedString = resultText.getText().toString();
				_rId = CategoryRadio.catGroup.getCheckedRadioButtonId();
				switch (_rId) {
				case 1:
					Storage.storeStringFile(_context, "Inspirational",
							savedString, false);

					break;
				case 2:
					Storage.storeStringFile(_context, "Love", savedString, true);

					break;
				case 3:
					Storage.storeStringFile(_context, "Friendship",
							savedString, false);

					break;

				default:
					break;
				}

			}
		});

		ll.addView(downloadButton);

		// Create Result Text
		resultText = new TextView(this);
		resultText.setTextSize(15);
		resultText.setPadding(5, 5, 5, 5);
		ll.addView(resultText);

		setContentView(ll);

	}

	public static void showResultToast() {
		Toast toast = Toast.makeText(_context, "Complete", Toast.LENGTH_SHORT);
		toast.show();
	}

}
