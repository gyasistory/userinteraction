package com.gyasistory.Layouts;

import android.content.Context;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.gyasistory.quotes.Categories;


public class CategoryRadio {
	
	public static RadioGroup catGroup;
	public static String _desc;
	
	public static Boolean CategoryRadio(Context context) {
		// Create Group
		catGroup = new RadioGroup(context);
		int i = 1;
		
		for(Categories name : Categories.values()){
			
			RadioButton b = new RadioButton(context);
			b.setText(name.toString() );
			b.setId(i);
			catGroup.addView(b);
			i+=1;
		}
		return true;
	}

}
