package com.gyasistory.lib;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

public class Storage {
	/**
	 * This will be used to create a internal and external storage of the Quote
	 * 
	 * 
	 * @param context
	 * @param filename
	 * @param content
	 * @param external
	 * @return
	 */
	@SuppressWarnings("resource")
	public static Boolean storeStringFile(Context context, String filename, String content, Boolean external){
		try {
			File file;
			FileOutputStream fos;
			if (external){
				file = new File(context.getExternalFilesDir(null), filename);
				fos = new FileOutputStream(file);
			}else {
				fos = context.openFileOutput(filename, Context.MODE_PRIVATE);
			}
			fos.write(content.getBytes());
			fos.close();
			Toast cm = Toast.makeText(context, "Saved", Toast.LENGTH_SHORT);
			cm.show();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return true;
	}
	
	public static String readString(Context context, String filename, Boolean external){
		String content = "";
		try{
			File file;
			FileInputStream fin;
			if (external){
				file = new File(context.getExternalFilesDir(null), filename);
				fin = new FileInputStream(file);
				
			}else {
				file = new File(filename);
				fin = context.openFileInput(filename);			
			}
			BufferedInputStream bin = new BufferedInputStream(fin);
			byte[] contentBytes = new byte[1024];
			int byteRead = 0;
			StringBuffer contentBuffer = new StringBuffer();
			while((byteRead = bin.read(contentBytes)) != -1){
				content = new String(contentBytes, 0, byteRead);
				contentBuffer.append(content);
			}
			content = contentBuffer.toString();
			fin.close();

		}catch(FileNotFoundException e){
			Log.e("Read Error", "File Not Found " + filename);
			Toast et = Toast.makeText(context, "No Data Saved for \r\n" + filename, Toast.LENGTH_LONG);
			et.show();
		}catch (Exception e) {
			Log.e("Read Error", "I/O Error");
		}
		return content;
	}
}
