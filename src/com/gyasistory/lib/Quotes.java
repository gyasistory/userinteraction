/**
 * 
 */
package com.gyasistory.lib;

import android.text.format.DateFormat;

/**
 * @author Gyasi Story
 *
 *This will get the author, Date, Quote, Text and Category from a Quote
 */


public interface Quotes {
	
	
	
	//set The Date of the Author
	public boolean setDate(DateFormat quoteDate);
	
	//set The Quote Text
	public boolean setQuoteText(String quoteText);
	
	//set The Category
	public boolean setCategory(String category);
	
	
	
	//get Date
	public DateFormat getDate();
	
	//get Quote Text
	public String getQuoteText();
	
	//get Category
	public String getCategory();

}
