package com.gyasistory.lib;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;

import com.gyasistory.userinteraction.MainActivity;

public class WebFileStuff {
	
	public static Boolean _conn = false;
	static String _connType = "Unavailable";
	public static String _tag;
	public static String _rssURL;
	public static String _results = "";
	
	public static String getConnType(Context context){
		netInfo(context);
		Log.i("Type", _connType);
		return _connType;
	}
	
	public static Boolean getConnStatus (Context context){
		netInfo(context);
		Log.i("Status", "Connected: " + _conn.toString());
		return _conn;
	}
	
	private static void netInfo(Context context){
		ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo ni = cm.getActiveNetworkInfo();
		if(ni != null){
			if(ni.isConnected()){
				_connType = ni.getTypeName();
				_conn = true;
			
			}
		}
	}
	
	/**
	 * @param url
	 * @return
	 */
	public static String getURLResponse(URL url){
		String response = "";
		try {
			URLConnection conn = url.openConnection();
			BufferedInputStream bin = new BufferedInputStream(conn.getInputStream());
			byte[] contextByte = new byte[1024];
			int byteRead = 0;
			StringBuffer responseBuffer = new StringBuffer();
			
			while((byteRead = bin.read(contextByte)) != -1){
				response = new String(contextByte,0,byteRead);
				responseBuffer.append(response);
			}
			Log.i("Response Test", response);
			return responseBuffer.toString();
		} catch (IOException e) {
			Log.e("URL Response Error", "getURLStringResponse");
		}
		Log.i("Response Test", response);
		return response;
		
	}
	
	public static String getFeedInfo(String rssURL, String tag){
		_rssURL = rssURL;
		_tag  = tag;
		getFeedSync async = new getFeedSync();
		async.execute(_rssURL);
		Log.i(_tag, _results);
		return _results;
	
	}
	
	static class getFeedSync extends AsyncTask<String, Void, String>{

		@Override
		protected String doInBackground(String... urls) {
			try {
				URL url = new URL(_rssURL);
				Log.i("RSS Feed", _rssURL);
				DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
				DocumentBuilder builder = factory.newDocumentBuilder();
				Document doc = builder.parse(url.openStream());
				
				NodeList list = doc.getElementsByTagName(_tag);
				
				for(int i = 0; i < list.getLength(); i++){
					Element item = (Element) list.item(i);
					//Log.i(_tag, item.getFirstChild().getNodeValue() );
					_results = item.getFirstChild().getNodeValue();
				}
				
			} catch (ParserConfigurationException e) {
				
				_results = "Parser Configuration Exception";
			} catch (SAXException e) {
				Log.e("ERROR", e.toString());
				_results = "SAX Exception";
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				Log.e("ERROR", e.toString());
				_results = "IO Exception";
				
			}
			//Log.i(_tag, _results);
			return _results;
			
		}
		
		@Override
		protected void onPostExecute(String result){
			MainActivity.showResultToast();
		}
	}
	

}
