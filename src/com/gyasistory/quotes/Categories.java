package com.gyasistory.quotes;

public enum Categories {
	Inspirational(1, "This are Quote that will inspire you"),
	Love(2, "These quotes are to affect the love with in you"),
	Friendship(3, "These quotes are to inspire the friendship in you.");
	
	private final int catId;
	private final String catDesc;
	
	private Categories(int id, String desc) {
		// Set the variable
		this.catId = id;
		this.catDesc = desc;
	}
	
	
	public int setId(){
		return catId;
	}
	
	public String setDesc(){
		return catDesc;
	}
}
