package com.gyasistory.quotes;

import android.text.format.DateFormat;
import android.util.Log;

import com.gyasistory.lib.Quotes;
import com.gyasistory.lib.WebFileStuff;

/**
 * this will load the  Value for the Friendship Quotes
 * 
 * @author gstory
 *
 */
public class Friendship implements Quotes {
	String inspRss = "http://www.quotesdaddy.com/feed/tagged/Friendship";
	public static String quoteDesc = "";

	@Override
	public boolean setDate(DateFormat quoteDate) {

		return false;
	}

	public boolean setQuoteText(String quoteText) {
		
		quoteDesc = WebFileStuff.getFeedInfo(quoteText, "title");
		Log.i("Results", quoteDesc);
		return true;
	}

	@Override
	public boolean setCategory(String category) {
		// TODO Auto-generated method stub
		return false;
	}

	
	@Override
	public DateFormat getDate() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getQuoteText() {
		setQuoteText(inspRss);
		
		return quoteDesc;
	}

	@Override
	public String getCategory() {
		// TODO Auto-generated method stub
		return null;
	}

}
